import { AES, enc } from "./crypto-ts.js";
var retry = false;

//Executes backgroundcheck when chrome starts and user enabled it
chrome.runtime.onStartup.addListener(function(){
	startup();
});

//Executes backgroundcheck when extension is reloaded or updated
chrome.runtime.onInstalled.addListener(function(details){
	startup();
});

//Schedules the backgroundcheck every 30min
chrome.alarms.onAlarm.addListener(function(alarm){
	if(alarm.name === "checkUnavailableAnimes"){
		chrome.storage.local.get(["disableBackgroundChecks", "firstuse"], function(result) {
			if(result.disableBackgroundChecks !== undefined && result.firstuse !== undefined){
				var disableBackgroundChecks = (AES.decrypt(result.disableBackgroundChecks, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8) === "true");
				var firstuse = (AES.decrypt(result.firstuse, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8) === "true");
				if(!disableBackgroundChecks && !firstuse){
					checkUnavailableAnimes();
				}
				chrome.alarms.create("checkUnavailableAnimes", {delayInMinutes: 30});
			}
		});
	}
});

function startup(){
	chrome.contextMenus.create({id: "crs-open-new-tab", title: "Open Crunchysync in a new tab", contexts: ["action"]}, () => {
    });
    chrome.contextMenus.onClicked.addListener(function(info){
        if (info.menuItemId == "crs-open-new-tab") {
            chrome.tabs.create({url: "index.html"});
        }
    });
	chrome.alarms.create("checkUnavailableAnimes", {delayInMinutes: 30});
	chrome.storage.local.get(["disableBackgroundChecks", "firstuse"], function(result) {
		if(result.firstuse !== undefined){
			if(result.disableBackgroundChecks !== undefined){
				var disableBackgroundChecks = (AES.decrypt(result.disableBackgroundChecks, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8) === "true");
				var firstuse = (AES.decrypt(result.firstuse, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8) === "true");
				if(!firstuse){
					if(!disableBackgroundChecks){
						checkUnavailableAnimes();
					}
				}else{
					chrome.tabs.create({url: "index.html"});
				}
			}
		}else{
			chrome.tabs.create({url: "index.html"});
		}
	});
}

function authenticate(){
    var locale = chrome.i18n.getUILanguage;
	var sessionid = "";
	chrome.storage.local.get(["username", "password", "deviceid"], function(result) {
		var username = AES.decrypt(result.username, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8);
		var password = AES.decrypt(result.password, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8);
		var deviceid = AES.decrypt(result.deviceid, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8);
		if(username != "" && password != ""){
            fetch("https://api.crunchyroll.com/start_session.1.json?"+
            "&device_type=com.crunchyroll.crunchyroid"+
            "&access_token=WveH9VkPLrXvuNm"+
            "&device_id=" + deviceid +
            "&version=457", {method: 'POST'}).then((response) => {
                response.json().then((parsedResponse) => {
                    if(parsedResponse.code === "ok" && !parsedResponse.error){
                        sessionid = parsedResponse.data.session_id;
                        fetch("https://api.crunchyroll.com/login.0.json?"+
                        "&session_id=" + sessionid +
                        "&locale=" + locale + 
                        "&account=" + encodeURIComponent(username) +
                        "&password=" + encodeURIComponent(password), {method: 'POST'}).then((response) => {
                            response.json().then((parsedResponse) => {
                                if(parsedResponse.code === "ok" && !parsedResponse.error){
                                    chrome.storage.local.set({"sessionid": AES.encrypt(sessionid, "5HR*98g5a699^9P#f7cz").toString()});
                                    chrome.storage.local.set({"userIsPremium": AES.encrypt(parsedResponse.data.user.premium.toString(), "5HR*98g5a699^9P#f7cz").toString()});
                                    checkUnavailableAnimes();
                                    console.log("authenticated");
                                }else{
                                    error("Login failed! Please make sure your username and password is valid. Discription: " + parsedResponse.message);
                                }
                            });
                        });
                    }else{
                        error("Connection failed! Please try again later. Discription: " + parsedResponse.message);
                    }
                });
            });
		}else{
			error("No Credentials set! Please save your credentials in the settingstab.");
		}
	});
}

//Checks if there are changes between current queue and previous queue and notifys the user
function checkUnavailableAnimes(){
    var locale = chrome.i18n.getUILanguage;
	var animes = [];
	var unavailable = [];
	var available = [];
	var watching = [];
	var userIsPremium = false;
	chrome.storage.local.get(["sessionid", "unavailable", "userIsPremium"], function(result) {
		if(result.userIsPremium !== undefined){
			userIsPremium = (result.userIsPremium === "true");
		}
		fetch("https://api.crunchyroll.com/queue.0.json?"+
        "&fields=most_likely_media,series,series.name,series.media_count,series.series_id,media.description,media.collection_name,media.collection_id,media.media_id,media.available_time,media.free_available_time,media.name,media.url,media.episode_number,series.url,media.screenshot_image,media.duration,media.playhead,media.premium_only,image.fwide_url"+
        "&media_types=anime|drama" +
        "&locale=" + locale +
        "&session_id=" + AES.decrypt(result.sessionid, '5HR*98g5a699^9P#f7cz').toString(enc.Utf8)).then((response) => {
            response.json().then((parsedResponse) => {
                if(parsedResponse.code === "ok" && !parsedResponse.error){
                    if(result.unavailable !== undefined || result.unavailable !== {}){
                        animes = parsedResponse.data;
                        animes = animes.filter(anime => anime.most_likely_media !== undefined);
                        animes.sort((co1, co2) => co1.most_likely_media.collection_name.localeCompare(co2.most_likely_media.collection_name));
                        watching = animes.filter(anime => (anime.most_likely_media.playhead < anime.most_likely_media.duration - 10 || anime.most_likely_media.duration == 0) && (anime.most_likely_media.playhead > 0 || anime.most_likely_media.episode_number != 1));
                        chrome.storage.local.set({"animes": animes});
                        unavailable = animes.filter(anime => anime.most_likely_media.playhead >= anime.most_likely_media.duration - 10);
                        if(!userIsPremium){
                            unavailable = unavailable.concat(animes.filter(anime => (anime.most_likely_media.playhead <= anime.most_likely_media.duration - 10) && anime.most_likely_media.premium_only));
                        }
                        chrome.storage.local.set({"unavailable": unavailable});
                        unavailable = result.unavailable;
                        var a = unavailable.length;
                        while(a--){
                            var unavailableAnime = unavailable[a];
                            var b = animes.length;
                            while(b--){
                                var anime = animes[b];
                                if(anime.most_likely_media !== undefined){
                                    if(anime.most_likely_media.playhead === undefined){
                                        anime.most_likely_media.playhead = 0;
                                    }
                                    if(anime.series.series_id === unavailableAnime.series.series_id){
                                        if(anime.most_likely_media.media_id === unavailableAnime.most_likely_media.media_id){
                                            if(!(anime.most_likely_media.playhead >= unavailableAnime.most_likely_media.duration - 10)){
                                                if((!anime.most_likely_media.premium_only && unavailableAnime.most_likely_media.premium_only) && !userIsPremium){
                                                    available.push(anime);
                                                }
                                            }
                                            animes.splice(b, 1);
                                            break;
                                        }else{
                                            if(anime.most_likely_media.episode_number > unavailableAnime.most_likely_media.episode_number){
                                                if(!anime.most_likely_media.premium_only || userIsPremium){
                                                    available.push(anime);
                                                }
                                            }
                                        }
                                        animes.splice(b, 1);
                                        break;
                                    }
                                }
                            }
                        }
                        available.forEach((anime) => {
                            chrome.notifications.create(anime.most_likely_media.url, {type: "image", iconUrl: "assets/images/crunchysync.png", imageUrl: anime.most_likely_media.screenshot_image.fwide_url, title: "A new Episode of " + anime['most_likely_media']['collection_name'] + " is available", message: anime.most_likely_media.name + "\nEpisode Nr." + anime.most_likely_media.episode_number});
                        });
                        chrome.notifications.onClicked.addListener(function(notificationId) {
                            chrome.tabs.create({url: notificationId});
                        });
                        if(watching.length > 0){
                            chrome.action.setBadgeBackgroundColor({ color: [247, 140, 37, 1] });
                            chrome.action.setBadgeText({text: watching.length+""});
                        }else{
                            chrome.action.setBadgeText({text: ""});
                        }
                    }
                }else{
                    if(!retry){
                        retry = true;
                        authenticate();
                    }else{
                        error(parsedResponse.message);
                    }
                }
            });
        });
	});
}

function error(message){
	chrome.notifications.create({type: "basic", iconUrl: "assets/icons/crunchysync.png", title: "Crunchysync encountered an error", message: message});
}